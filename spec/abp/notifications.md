# The Adblock Plus notification system

Adblock Plus has a system for showing various types of notifications to users under certain conditions.

- [What's missing from this document](#whats-missing-from-this-document)
- [Notification types](#notification-types)
- [Display methods](#display-methods)
- [Notification content](#notification-content)
- [Targeting options](#targeting-options)
- [Notification sources](#notification-sources)
- [Notification repository format](#notification-repository-format)

## What's missing from this document

The notification system is quite extensive, a few things are still missing from this document:

**Notification categories** - There is a mechanism to group notifications to allow the user to opt-out of only notifications of the same category.

**Notification groups** - There is a mechanism for having notifications only appear for a random subset of the user base.

## Notification types

The notification type indicates which kind of notification should be shown and thereby how it should be displayed and behave.

| Severity | Type | [Persistent](#persistence) | Affected by [opt-out](#opt-out) |
|-|-|-|-|
|3|critical|yes|no|
|2|relentless|no|no|
|1|question|no|yes|
|0|information|no|yes|
|-|normal|no|yes|
|0|(default)|no|yes|

### Severity

ABP never shows the user more than one notification at a time. If multiple notifications are supposed to be shown, the notification with the highest _severity_ is shown first. If multiple notifications have the same _severity_, the first notification in the list is shown first.

After the first notification has been closed, the next notification will be shown the next time its condition is met (i.e. after the next update check or as soon as its _URL filters_ match). Note that critical notifications will show up each time their condition is met, and will therefore prevent all other notifications from being shown.

### Persistence

Any non-persistent notifications will be shown only once unless an [interval](#interval) is specified. Persistent notifications will be shown each time a [notification check](#update-check-frequency) happens.

A notification is being considered shown as soon as the extension selects it and before it is being displayed to the user. Note that question notifications are only considered shown after the user clicks them away.

## Display methods

Display methods are assigned based on the notification type as follows:

| [Type](#notification-types) | [Icon](#toolbar-icon) | [Web notification](#web-notification) | [Popup notification](#popup-notification) |
|-|-|-|-|
|critical|`!`|yes|`#CA0000`|
|relentless|no|yes|no|
|question|no|yes|no|
|information|`?`|no|`#0797E1`|
|normal|no|yes|no|
|(default)|no|no|`#E6E6E6`|

Display methods are limited in which parts of a notification they can contain:

| Display method | [Type](#notification-types) | Text | [Links](#links) | [Opt-out](#opt-out)[1] |
|-|-|-|-|-|
|[Popup notification](#popup-notification)|yes|yes|yes|yes|
|[Toolbar icon](#toolbar-icon)|yes|no|no|no|
|[Web notification](#web-notification)|no|partial[2]|partial[2]|yes|

1. Not shown for [notification types](#notification-types) that are unaffected by the opt-out.
2. Depending on the browser and OS.

### Toolbar icon

The extension icon is shown in the browser's toolbar and can transition to and from a different icon.

![](../../res/abp/notifications/icon-critical.png)

### Web notification

[Web notifications](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API) are windows that appear detached from the browser. Their appearance differs depending on the browser and OS the user is on.

An additional button will be added for each link. The label of the button will contain the content of the corresponding `<a>` tag from the message. If the button is clicked the link will be opened in a new tab.

![](../../res/abp/notifications/notification-link.png)

If the notification contains more links than it can display, it will instead only add a single button whose label will be `Open all links from this notification`. If that button is clicked all links will be opened in new tabs.

![](../../res/abp/notifications/notification-links.png)

Web notifications can be closed by clicking the _X_ icon.

Regardless of how many links the notification contains, it will always reserve one button for [disabling notifications](#opt-out) and label it `Configure notification settings`.

### Popup notification

Popup notifications are shown as part of the Bubble UI.

![](../../res/abp/notifications/popup-information-links.png)

Popup notifications can be closed by clicking _Close_ which will return the [toolbar icon](#toolbar-icon) and [popup notification](#popup-notification) to their original state.

Clicking on _Stop showing notifications_ will do the same, but also [disables notifications](#opt-out).

## Notification content

### Links

Notifications can contain [documentation links](/spec/adblockplus.org/documentation-link.md), see [Links](#links-1) below.

Clicking the link will open the [documentation links](/spec/adblockplus.org/documentation-link.md) in a new tab but not close the notification.

## Behaviors

### Interval

Notifications can reappear in a given interval which is specified in milliseconds.

Cannot be applied to notifications with type "critical".

### Opt-out

Notifications can be disabled by choosing the respective option in a notification.

This can be changed in the [Customize section](options-page.md#customize-section) on the options page via the _Show useful notifications_ option.

## Targeting options

A notification will not be shown unless all targeting options of a target match.

- Browser UI locales
- Browser version range
- Extension version range
- Number of blocked requests
- OS version range
- URL filters to trigger notifications when visiting certain URLs

## Notification sources

Notifications can be added by ABP while it's running. However, it also regularly checks the backend for notifications to show, which are defined in a file called `notification.json`. Since many notifications show up immediately after they have been downloaded, it is important to understand how this works.

#### New users

For new users, notifications get downloaded one minute after the installation.

#### Existing users

For existing users, notifications get downloaded roughly once per day, but it depends. The logic is as follows:

##### Update check frequency

One minute after starting ABP (e.g. the browser), and then every 60 minutes, ABP checks whether it needs to update notifications.

##### Update check logic

    if last update failed and was less than 24 hours ago then
      end check

    if last update was more than 48 hours ago then
    update immediately, schedule next update in 24 hours
    end check

    if last check was more than 24 hours ago then
    delay the next update's scheduled time by the time that passed since the last check
    end check

    if the scheduled time for the next update has passed then
    update immediately, schedule next update in 24 hours
    end check

## Notification repository format

The notifications are stored in the [notification repository](https://hg.adblockplus.org/notifications/).

The notification repository format uses simple key value pairs. `Title` and `Message` are mandatory, the `severity` will default to *information* if omitted.


***NOTE***: The examples use javascript regular expressions do give an idea which values are valid, they don't necessarily define what is correct.

```javascript
/*
severity = normal

title.en-US = Test
message.en-US = Test
*/

severity = /^(information|normal|critical|relentless)$/
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```
### Attributes

#### Severity

The severity (type) of the notification. Will default to *information* if omitted.

```javascript
// severity = Normal
severity = /^(information|normal|critical|relentless)$/
```

#### Title

The title of the notification. The title can be specified in multiple locales.
The default locale *en-US* is mandatory and must always be specified.

```javascript
// title.en-US = Title
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Message

The message of the notification. The message can be specified in multiple locales.
The default locale *en-US* is mandatory and must always be specified.

```javascript
// message.en-US = message
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Inactive

Don't show notification if inactive

```javascript
// inactive = yes
// inactive = no
inactive = /^(yes|no)$/
```

#### Variants

You can specify multiple variants and define the distribution across users
via the `sample` argument. The sample argument defines in percent (0.5 = 50%)
how many users will get this variant of the notification. If the percentages of
the variants don't add up (to 100%), the base notification will be distributed
in the remaining cases.


```javascript
/*
title.en-US = Base
message.en-US = Base

[0]
sample 0.25
title.en-US = Variant0
message.en-US = Variant0
[0]
sample 0.5
title.en-US = Variant1
message.en-US = Variant1
*/

title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/

[\d+]
sample = /^\d+(|\.|\.\d+)$/
title./^[\w]{2}-[\w]{2}$/ = /^.+$/
message./^[\w]{2}-[\w]{2}$/ = /^.+$/
```

#### Interval

Interval in milliseconds. Notification is shown every interval. Note regardless
of interval the notification will not be shown more often than
[update check frequency](#update-check-frequency).

#### Links

Space seperated list of [documentation links](/spec/adblockplus.org/documentation-link.md). For each entry there has to be a corresponding ```<a>``` tag in the message, the ```<a>``` tag will determine which part of the message will be linked (just like a normal ```<a>``` tag). If there are less ```<a>``` tags than links the superfluous links are ignored. If there are more ```<a>``` tags than links the superfluous ```<a>``` tags are ignored.

```javascript
/*
title.en-US = Title
message.en-US = Message with a <a>link</a>
links = chrome_support

title.en-US = Title
message.en-US = Message with a <a>link</a> and another <a>link2</a>
links = chrome_support knownIssuesChrome_filterstorage
*/
links = /([\w-_]+)+/
```
### Filters

It's possible to limit the users the notification will be shown to by using additional filters. A notification will show up if all filters are satisfied.

#### Target

A space seperated list of filters to only show notification to specific users.

Extension refers to the name and version of the browser extension (e.g. adblockplus).
Application refers to the name and version of the the browser (e.g. chrome).
Platform refers to the browser platform (e.g. chromium).
Locales show notifications based on the locale of the user
BlockedTotal shows notification based on the total number of ads blocked (*Prefs.blocked_total*).

```javascript
// target = extension=adblockplus extensionVersion=2.3.0.3702
// target = application=chrome applicationVersion<=1.5.0.964
target = /^(extension|application|platform)=\w+ \1Version(=|<=|=>)[\.\d]+$/
// target = locales=en-US
// target = locales=en-US,de-DE
target = /^locales=([\w]{2}-[\W]{2}[,]*)+$/
// blockedTotal=10
// blockedTotal>=10
// blockedTotal<=10
target = /^blockedTotal(=|<=|=>)\d+$/
// target = extension=adblockplus extensionVersion=2.3.0.3702 locales=en-US blockedTotal>=1000
```

If multiple targets are specified only one needs to be (fully) satisfied for the notification to show up.

```javascript
// target = (first AND second)
//                 OR
// target = (third AND forth)
target = application=chrome applicationVersion>=60.0
target = application=firefox applicationVersion>=55.0
```

#### Start

Don't show the notification before `start`.

```javascript
// start = '2017-07-24T12:00'
start = /^[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}$/;
```


#### End

Don't show the notification after `end`.

```javascript
// end = '2017-07-24T12:00'
end = /^[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}$/;
```
#### URLs

A space separated list of domains. The notification will only shown if the
users visits one of the specified domains.

```javascript
// urls = example.com
// urls = example.com foo.com bar.com
urls = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/
```

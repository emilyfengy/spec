# Developer tools panel

Developer tools panel of the Adblock Plus browser extension.

The developer tools panel is used by advanced users to find out how the extension affects the inspected web page.

![](/res/abp/devtools-panel/overview.png)

Title: `Adblock Plus`

1. [Header](#header)
1. [Footer](#footer)
1. [List](#list)
1. [Search](#search)
1. [Content types](#content-types)

Colors should adjust accordingly based on whether the "light" or "dark" developer tools theme is being used.

## Header

`Show [1] items of [2] type`

[1]: [Status dropdown](#status-dropdown)

[2]: [Content type dropdown](#content-type-dropdown)

### Status dropdown

Contains the values:

- `all` (default)
- `blocked`
- `whitelisted`

Whenever the value changes, hide all items in [the list](#list) whose status doesn't match the selected value. Don't hide any if the selected value is `all`.

### Content type dropdown

Contains the values:

- `any` (default)
- see [content types](#content-types)

Whenever the value changes, hide all items in [the list](#list) whose content type doesn't match the selected value. Don't hide any if the selected value is `any`.

## Footer

![](/res/abp/devtools-panel/footer.png)

`[Reload][1] page to see effect of filter changes`

[1]: Reloads the inspected tab

The footer is hidden unless a filter has changed that relates to one of the items in [the list](#list).

## List

1. [Styles](#styles)
1. [Request column](#request-column)
1. [Type column](#type-column)
1. [Filter column](#filter-column)
1. [Actions](#actions)

### Styles

Add new items to the list for each request that is made on the inspected tab. Also add new items whenever an element hiding filter is applied on an element in the inspected tab. Update or remove existing items accordingly when filters change. Remove all items from the list when the inspected tab reloads.

Secondary text such as subtitles should have an opacity of `60%`.

Text color changes depending on list item's status:

- Blocked: `#F00`
- Whitelisted: `#0F0`

Text uses italics after list item has changed. Same applies to filter list title placeholder strings.

Text is truncated with an ellipsis if it would exceed the available space.

### Request column

Primary title: `Request`

Secondary title: `Document domain`

Primary content: Request URL if the list item corresponds to an HTTP(S) request. Leave empty otherwise. If the URL got rewritten by a filter show the text `[1] rewritten to [2]` instead.

[1]: Original URL

[2]: Rewritten URL

While hovering over a URL, the URL should be underlined. Clicking on a URL should open it in a developer tools panel. In case that's not possible, open the URL in a new tab instead.

Secondary content: Referrer domain

### Type column

Primary title: `Type`

Primary content: [Content type](#content-types) of corresponding HTTP(S) request. Set to `ELEMHIDE` otherwise.

### Filter column

Primary title: `Filter`

Secondary title: `Origin`

Primary content: Text of the filter that was applied to the list item. Leave empty if no filter was applied.

Secondary content: Filter list title corresponding to applied filter. Set to `user-defined` if custom filter was applied. Set to "unnamed subscription" if filter list doesn't have a title. Leave empty if no filter was applied.

### Actions

While hovering over a list item, one or more buttons should be shown in the corresponding list item's [Filter column](#filter-column).

- [`Add exception`](#add-exception)
- [`Block item`](#block-item)
- [`Remove rule`](#remove-rule)

![](/res/abp/devtools-panel/actions.png)

#### Add exception

When to show: If status is blocked unless content type is `ELEMHIDE`.

On click: Adds a new filter that unblocks the request.

#### Block item

When to show: If no filter was applied unless content type is `ELEMHIDE`.

On click: Adds a new filter that blocks the request.

#### Remove rule

When to show: If custom filter was applied.

On click: Removes custom filter that was applied.

## Search

Should integrate with the browser's integrated search functionality [where possible](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/devtools.panels/ExtensionPanel#Browser_compatibility).

![](/res/abp/devtools-panel/search.png)

When performing a search, any list item that doesn't match the query should be hidden. To determine whether a list item matches a query, it needs to match parts of at least one of the following:

- Primary content in [Request column](#request-column)
- Primary content in [Type column](#type-column)
- Primary content in [Filter column](#filter-column)
- Secondary content in [Filter column](#filter-column)

## Content types

Depending on the browser and browser version, the list of available content types may include any of the ones below. `OTHER` should be the last item in the list.

- `BACKGROUND`
- `CSP`
- `DOCUMENT`
- `DTD`
- `ELEMHIDE`
- `FONT`
- `GENERICBLOCK`
- `GENERICHIDE`
- `IMAGE`
- `MEDIA`
- `OBJECT`
- `OBJECT_SUBREQUEST`
- `OTHER`
- `PING`
- `POPUP`
- `SCRIPT`
- `STYLESHEET`
- `SUBDOCUMENT`
- `WEBRTC`
- `WEBSOCKET`
- `XBL`
- `XMLHTTPREQUEST`

Source: [adblockpluscore/lib/filterClasses.js](https://hg.adblockplus.org/adblockpluscore/file/8b4d39d03313/lib/filterClasses.js#l857)
